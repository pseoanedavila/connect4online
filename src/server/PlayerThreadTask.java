package server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.net.Socket;
import java.io.EOFException;

public class PlayerThreadTask implements Runnable{
	//Attributes

	private DataOutputStream dos;
	private DataInputStream dis;
	private Socket client;
	private Connect4Server game;
	private short playerID;
	private short lastModifiedPosition = -1; //The opponent's last modified position

	//Constructor
	
	public PlayerThreadTask(Socket client, Connect4Server game, short playerID) throws Exception {
		this.client = client;
		this.dos = new DataOutputStream(client.getOutputStream());
		this.dis = new DataInputStream(client.getInputStream());
		this.playerID = playerID;
		this.game = game;
	}
	
	//Public methods
	
	public void run() {
		try {
			mainLoop();
		} catch (Exception e) {
			try {
				client.close();
			} catch (Exception e2) {
				return;
			}
		}
	}

	//Private methods
	
	private void mainLoop() throws Exception {
		while(true) {
			try {
				switch (dis.readShort()) {
					case 0:
						sendResponse(true, playerID);
						break;
					case 1: 
						doMove(dis.readShort(), dis.readShort());
						break;
					case 2:
						getMove(dis.readShort());
						break;
					case 3:
						getStatus();
						break;
					default:
						sendResponse(false, (short)-3); //-3: Incorrect petition
				}
			} catch (EOFException e) {
				continue;
			}
		}
	}
	
	private void doMove(short playerID, short move) throws Exception {
		short positionChanged = game.doMove(move, playerID);
		sendResponse(positionChanged >= 0, positionChanged);
		if (positionChanged >= 0) {
			synchronized(game.threadCondition) {
				game.threadCondition.notify();
			}
		}
	}
	
	private void getMove(short playerID) throws Exception {
		short positionChanged = game.getMove(playerID);
		if (playerID != this.playerID) {
			while (positionChanged == lastModifiedPosition) {
				synchronized(game.threadCondition) {
					game.threadCondition.wait(5000);
					positionChanged = game.getMove(playerID);
				}
			}
			lastModifiedPosition = positionChanged;
		}
		sendResponse(positionChanged >= 0, positionChanged);
	}
	
	private void getStatus() throws Exception {
		short gameStatus = game.getStatus();
		sendResponse(gameStatus >= 0, gameStatus);
	}
	
	private void sendResponse(boolean status, short response) throws Exception {
		this.dos.writeBoolean(status);
		this.dos.writeShort(response);
	}

}
