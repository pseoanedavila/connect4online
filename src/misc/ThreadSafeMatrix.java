package misc;

public class ThreadSafeMatrix {
	//MARK: Attributes
	
	private short[][] grid;
	private Object[][] mutex;
	private final short MAX_X;
	private final short MAX_Y;
	
	//MARK: Constructor
	
	public ThreadSafeMatrix(short x, short y) {
		MAX_Y = y;
		MAX_X = x;
		grid = new short[MAX_X][MAX_Y];
		mutex = new Object[MAX_X][MAX_Y];
		initMutex();
	}
	
    public void updateAt(short value, short i, short j) {
    	synchronized(mutex[i][j]) {
    		grid[i][j] = value;
    	}
    }
    
    public short elementAt(short i, short j) {
    	short value;
    	synchronized(mutex[i][j]) {
    		value = grid[i][j];
    	}
    	return value;
    } 
    
    public void initMutex() {
    	short i, j;
    	for (i = 0; i < MAX_X; i++) {
    		for (j = 0; j < MAX_Y; j++) {
    			mutex[i][j] = new Object();
    		}
    	}
    }
    
    public void setAllValuesTo(short value) {
    	short i, j;
    	for (i = 0; i < MAX_X; i++) {
    		for (j = 0; j < MAX_Y; j++) {
    			synchronized(mutex[i][j]) {
    				grid[i][j] = value;
    			}
    		}
    	}
    }
}
