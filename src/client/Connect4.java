package client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

import rmi.*;

public class Connect4 implements Connect4I{
	//Attributes
	private Socket server;
	private OutputStream os;
	private InputStream is;
	private DataOutputStream dos;
	private DataInputStream dis;
	//Constructors
	public Connect4() throws Exception {
		try {
			this.server = new Socket("localhost", 9999);
			this.os = this.server.getOutputStream();
			this.is = this.server.getInputStream();
			this.dos = new DataOutputStream(this.os);
			this.dis = new DataInputStream(this.is);
		} catch (Exception e){
			System.out.println("Error while connecting to server");
			throw e;
		}
	}
	
	//Public methods
	
	//Returns the playerID if everything went ok, -1 if not
	public short startGame() throws Exception {
		try {
			//Tell the server we want to start a new game
			System.out.println("I wanna start a new game");
			sendShort((short)0);
			boolean petitionOk = this.dis.readBoolean();
			short response = this.dis.readShort();
			return responseShort(petitionOk, response);
		} catch (Exception e) {
			System.out.println("Network error");
			throw e;
		}
	}
	
	public short doMove(short move, short playerID) throws Exception {
		try {
			sendShort((short)1);
			sendShort(playerID);
			sendShort(move);
			boolean petitionOk = this.dis.readBoolean();
			short response = this.dis.readShort();
			return responseShort(petitionOk, response);
		}
		catch (Exception e) {
			System.out.println("Network error");
			throw e;
		}
	}
	
	public short getMove(short playerID) throws Exception {
		try {
			sendShort((short)2);
			sendShort((short)playerID);
			this.dis.readBoolean();
			short response = this.dis.readShort();
			return response;
		}
		catch (Exception e) {
			System.out.println("Network error");
			throw e;
		}
	}
	
	public short getStatus() throws Exception {
		try {
			sendShort((short)3);
			boolean petitionOk = this.dis.readBoolean();
			short response = this.dis.readShort();
			return responseShort(petitionOk, response);
		} catch (Exception e) {
			System.out.println("Network error");
			throw e;
		}
	}
	
	public void endSession() throws Exception {
		this.server.close();
		this.dis.close();
		this.dos.close();
	}
	
	//Private methods
	
	private void sendShort(short data) throws Exception {
		this.dos.writeShort(data);
		this.dos.flush();
	}
	
	private short responseShort(boolean petitionOk, short response) {
		if (petitionOk) return response;
		return -1;
	}
	
	
}
